const cards = [
  {index: '2', type: 'You', rule: 'Whoever drew the card assigns a drink'}
  ,{index: '3', type: 'Me', rule: 'Whoever drew the card drinks'}
  ,{index: '4', type: 'Floor', rule: 'Everyone races to touch the floor, last person to do so drinks'}
  ,{index: '5', type: 'Guys', rule: 'All guys drink'}
  ,{index: '6', type: 'Chicks', rule: 'All girls drink'}
  ,{index: '7', type: 'Heaven', rule: 'All players point towards the sky, last player to do so drinks'}
  ,{index: '8', type: 'Mate', rule: 'Pick a person to drink with'}
  ,{index: '9', type: 'Rhyme', rule: 'Say a phrase, and everyone else must say phrases that rhyme'}
  ,{index: '10', type: 'Categories', rule: 'Pick a category, and say something from that category (i.e. if "drinking games" was the category, "kings" would be a viable answer.'}
  ,{index: 'J', type: 'Never have I ever', rule: 'Each player puts up 3 fingers, then starting with the person who drew the card, each player says "never have I ever «something»". If you\'ve done it, you put a finger down, until someone loses  '}
  ,{index: 'Q', type: 'Questions', rule: 'The person who drew the card asks a random person a question, and they then turn and ask a random person a question, until someone loses by either failing to ask a question or by responding to the person who just asked them a question'}
  ,{index: 'K', type: 'Kings', rule: 'Make a rule that everyone must follow until the next King is drawn (i.e. force everyone to drink after each turn)'}
  ,{index: 'A', type: 'Waterfall', rule: 'Every player begins drinking, and no one can stop until the player before them does'}
];
const colors = [
  {name: 'spades', color: '#000000'}
  ,{name: 'hearts', color: '#ff0000'}
  ,{name: 'clubs', color: '#000000'}
  ,{name: 'diamonds', color: '#ff0000'}
];
const face = {
  eyes: ['eyes1', 'eyes10', 'eyes2', 'eyes3', 'eyes4', 'eyes5', 'eyes6', 'eyes7', 'eyes9'],
  nose: ['nose2', 'nose3', 'nose4', 'nose5', 'nose6', 'nose7', 'nose8', 'nose9'],
  mouth: ['mouth1', 'mouth10', 'mouth11', 'mouth3', 'mouth5', 'mouth6', 'mouth7', 'mouth9'],
  color: [
    {code: '1abc9c',light: false},
    {code: '2ecc71',light: false},
    {code: '3498db',light: false},
    {code: '9b59b6',light: false},
    {code: '34495e',light: false},
    {code: 'f1c40f',light: false},
    {code: 'e67e22',light: false},
    {code: '34495e',light: false},
    {code: 'e74c3c',light: false},
    {code: 'ecf0f1',light: true},
    {code: '95a5a6',light: false}
  ]
};

module.exports = {
  cards: cards,
  colors: colors,
  face: face
}
