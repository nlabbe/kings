import React from 'react';
import * as RoomCtx from '../Hoc/Room';
import * as UserCtx from '../Hoc/User';
import Cards from '../Cards';
import * as SizeCtx from '../Hoc/Size';
import Players from '../Players';
import Avatar from '../Avatar';
import Background from '../Common/Background';
import { H1, Big } from '../Common/Text';
import { AvatarWrapper } from './Styles';

class Game extends React.PureComponent {
  static defaultProps = {
  }

  state = {
  }

  render() {
    const { roomName, user, turn, replay, room, cards, width } = this.props;
    let canPlay = false;

    if(room && room.turn) {
      if(user && user.id === room.turn.id) {
        canPlay = true;
      }
    }

    return <React.Fragment>
      <Background color={room.turn ? room.turn.color.code : user.color.code}>
        <H1>{roomName}</H1>
        {room.turn && <React.Fragment>
          <AvatarWrapper>
            <Avatar
              {...room.turn}
              width={width / 2}
              center />
          </AvatarWrapper>
          <Big {...room.turn}>
            {room.turn.name}
          </Big>
        </React.Fragment>}
        {cards && <Cards
          {...room.turn}
          canPlay={canPlay}
          onDraw={(e, card) => !room.end ? turn && turn(e, card.id) : replay && replay(e)}
          center
          maxWidth={250}
          cards={cards} />}
      </Background>
    </React.Fragment>;
  }
}

export default SizeCtx.withSize(UserCtx.withUser(RoomCtx.withRoom(Game)));
