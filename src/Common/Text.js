import styled from 'styled-components';

const common = `
  pointer-events: none;
  user-select: none;
`;

export const H1 = styled.div`
  ${common}
  text-align: center;
  font-size: 24px;
  font-weight: bold;
  text-transform: uppercase;
`;

export const Div = styled.div`
  ${common}
  text-align: center;
  font-size: 12px;
`;

export const Big = styled.div`
  ${common}
  color: ${({ color }) => color.light ? 'black' : 'white'};
  text-align: center;
  font-size: 2rem;
`;
