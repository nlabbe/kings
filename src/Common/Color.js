import styled from 'styled-components';

export default styled.div`
  width: 50px;
  height: 50px;
  background: #${({ color }) => color.code};
  display: flex;
  bottom: 0px;
  flex-direction: row;
  flex: 1;
  cursor: pointer;
`;
