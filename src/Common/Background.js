import styled from 'styled-components';

export default styled.div`
  display: block;

  :before {
    content: ' ';
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    background: ${({ color }) => `#${color}`};
    z-index: 0;
  }
`;
