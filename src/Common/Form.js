import styled from 'styled-components';

export const Input = styled.input`
  position: relative;
  display: block;
  border: none;
  outline: none;
  margin: 10px auto;
  width: 100%;
  text-align: center;
  font-size: 24px;
  color: ${({ light }) => light ? 'black' : 'white'};
  background: transparent;
  padding: 0;
  margin: 0;
  line-height: 40px;

  ::placeholder {
    color: ${({ light }) => light ? 'black' : 'white'};
    opacity: 0.5;
  }
`;

export const Button = styled.a`
  position: relative;
  display: block;
  border: none;
  outline: none;
  margin: 10px auto;
  text-align: center;
  font-size: 24px;
  color: ${({ light }) => light ? 'black' : 'white'};
  background: transparent;
  line-height: 40px;
  max-width: 200px;
  padding: 10px;
  background: ${({ light }) => light ? 'white' : 'black'};
`;
