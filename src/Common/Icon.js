import React from 'react';
import styled from 'styled-components';

import Clubs from './icons/clubs.svg?sprite';
import Diamonds from './icons/diamonds.svg?sprite';
import Hearts from './icons/hearts.svg?sprite';
import Spades from './icons/spades.svg?sprite';

const icons = {
  Clubs: styled(({ className }) => <Clubs className={className} />)`
    fill: currentColor;
  `,
  Diamonds: styled(({ className }) => <Diamonds className={className} />)`
    fill: currentColor;
  `,
  Hearts: styled(({ className }) => <Hearts className={className} />)`
    fill: currentColor;
  `,
  Spades: styled(({ className }) => <Spades className={className} />)`
    fill: currentColor;
  `
};

export default icons;
