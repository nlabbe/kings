import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Wrapper } from './Styles';
import Card from './Card';

class Cards extends PureComponent {
  static propTypes = {
    center: PropTypes.bool,
    cards: PropTypes.object,
    maxWidth: PropTypes.number,
    color: PropTypes.object
  }

  render() {
    const { cards, center, maxWidth, canPlay, color } = this.props;

    let activeId = null;
    let previousId = null;
    Object.keys(cards).map((id) => {
      if(!activeId && cards[id].active === false) {
        activeId = previousId;
      }else {
        previousId = id;
      }
    });
    if(!activeId) {
      activeId = previousId;
    }

    return <Wrapper
    center={center}
    maxWidth={maxWidth}>
    {cards && Object.keys(cards).map((id) => <Card
      {...this.props}
      color={color}
      playable={canPlay && activeId === id}
      key={id}
      index={id}
      card={cards[id]} />)}
  </Wrapper>;
  }
}

export default Cards;
