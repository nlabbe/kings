import styled from 'styled-components';
import Icon from '../Common/Icon';

export const TextWrapper = styled.div`
  left: 20px;
  width: calc(100% - 40px);
  position: absolute;
  top: 50%;
  transform: translate(0, -50%);
`;

export const Title = styled.div`
  pointer-events: none;
  user-select: none;
  text-align: center;
  color: ${({ color }) => color};
  font-size: 18px;
  text-transform: uppercase;
`;

export const Description = styled.div`
  margin-top: 20px;
  pointer-events: none;
  user-select: none;
  text-align: center;
  color: ${({ color }) => color};
  font-size: 12px;
  line-height: 16px;
`;

export const Wrapper = styled(({ children, center, maxWidth, x, y, moving, active, playable, ...args }) => <div {...args}><div>{children}</div></div>)`
  &> div {
    position: relative;
    width: 100%;
    padding-top: 150%;
  }

  position: absolute;
  top: 50%;
  left: 50%;
  width: 100%;
  transform: translate(-50%, -50%);
  ${({ maxWidth }) => maxWidth && `max-width: ${maxWidth}px;`}
  ${({ center }) => center && 'margin: 0 auto;'}
`;

export const CardWrapper = styled(({ children, center, maxWidth, x, y, moving, active, defaultDeg, ...args }) => <div {...args}><div><div>{children}</div></div></div>)`
  touch-action: none;
  background: white;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  z-index: 1;
  box-shadow: 0px 1px 0px #00000005;
  border-radius: 15px;
  transition: transform ${({ moving }) => moving ? '0' : '1.5'}s ease;
  ${({ x, y, deg, scale }) => `transform: translate(${x}px, ${y}px) rotate(${deg}deg) scale(${scale});`}
`;

const iconStyle = `
  pointer-events: none;
  user-select: none;
  position: absolute;
  width: 20px;
  height: 20px;
  z-index: 2;
`;

export const Spades = styled(Icon.Spades)`
  ${iconStyle}
  ${({ position }) => position === 'right' ? 'top: 20px;' : 'bottom: 20px;'}
  ${({ position }) => position === 'right' ? 'right: 20px;' : 'left: 20px;'}
  color: ${({ color }) => color.code};
`;

export const Hearts = styled(Icon.Hearts)`
  ${iconStyle}
  ${({ position }) => position === 'right' ? 'top: 20px;' : 'bottom: 20px;'}
  ${({ position }) => position === 'right' ? 'right: 20px;' : 'left: 20px;'}
  color: ${({ color }) => color.code};
`;

export const Clubs = styled(Icon.Clubs)`
  ${iconStyle}
  ${({ position }) => position === 'right' ? 'top: 20px;' : 'bottom: 20px;'}
  ${({ position }) => position === 'right' ? 'right: 20px;' : 'left: 20px;'}
  color: ${({ color }) => color.code};
`;

export const Diamonds = styled(Icon.Diamonds)`
  ${iconStyle}
  ${({ position }) => position === 'right' ? 'top: 20px;' : 'bottom: 20px;'}
  ${({ position }) => position === 'right' ? 'right: 20px;' : 'left: 20px;'}
  color: ${({ color }) => color.code};
`;

export const Left = styled.div`
  pointer-events: none;
  user-select: none;
  position: absolute;
  top: ${({ padding }) => padding || 10}px;
  left: ${({ padding }) => padding || 10}px;
  font-size: 20px;
  color: ${({ color }) => color};
`;

export const Right = styled.div`
  pointer-events: none;
  user-select: none;
  position: absolute;
  bottom: ${({ padding }) => padding || 10}px;
  right: ${({ padding }) => padding || 10}px;
  font-size: 20px;
  color: ${({ color }) => color};
`;
