import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { CardWrapper, Spades, Hearts, Clubs, Diamonds, Left, Right, TextWrapper, Title, Description } from './Styles';

class Card extends PureComponent {
  static propTypes = {
    card: PropTypes.object,
    playable: PropTypes.bool,
    text: PropTypes.string,
    maxWidth: PropTypes.number,
    defaultDeg: PropTypes.number,
    onClick: PropTypes.func,
    center: PropTypes.bool
  }

  state = {
    move: this.move.bind(this),
    moving: false,
    startX: 0,
    startY: 0,
    x: 0,
    y: 0
  }

  constructor(props) {
    super(props);
    this.move = this.move.bind(this);
    this.end = this.end.bind(this);
    if(typeof window !== 'undefined') {
      document.addEventListener('mousemove', this.move);
      document.addEventListener('touchmove', this.move);
      document.addEventListener('mouseup', this.end);
      document.addEventListener('touchend', this.end);
    }
  }

  getIcon(color, position) {
    const { card } = this.props;

    switch(card.name) {
      case 'spades':
        return <Spades position={position} color={color} />
      case 'hearts':
        return <Hearts position={position} color={color} />
      case 'clubs':
        return <Clubs position={position} color={color} />
      case 'diamonds':
        return <Diamonds position={position} color={color} />
    }
  }

  points(e) {
    if(e.touches && e.touches && e.touches[0].clientX) {
      return {
        x: e.touches[0].clientX || 0,
        y: e.touches[0].clientY || 0
      }
    }
    return {
      x: e.clientX || 0,
      y: e.clientY || 0
    }
  }

  start(e) {
    e.preventDefault();
    const { x, y } = this.points(e);
    this.setState({moving: true, startX: x, startY: y});
  }
  move(e) {
    e.preventDefault();
    if(this.state.moving) {
      const { x, y } = this.points(e);
      this.setState({x: -(this.state.startX - x), y: -(this.state.startY - y)});
    }
  }
  end(e) {
    e.preventDefault();
    if(this.state.moving) {
      const { card } = this.props;
      const { x, y } = this.state;
      if(Math.abs(Math.max(x), y) > 60) {
        this.props.onDraw && this.props.onDraw(e, card);
        const w = x > 0 ? window.innerWidth : - window.innerWidth;
        const h = y > 0 ? window.innerHeight : - window.innerHeight;
        this.setState({moving: false, startX: e.clientX, startY: e.clientY, x: w, y: y});
      }else {
        this.setState({moving: false, startX: e.clientX, startY: e.clientY, x: 0, y: 0});
      }
    }
  }

  render() {
    const { card, maxWidth, center, playable, color } = this.props;
    const { x, y, moving } = this.state;

    let deg = Math.max(x, y);
    if(deg > 180) deg = 180;
    if(deg < -180) deg = -180;

    let scale = 1+Math.abs(Math.max(x, y))/100;
    if(scale > 2) scale = 2;

    if(playable) console.log('playable', playable);

    return <CardWrapper
      moving={moving}
      x={card.active ? x : window.innerWidth*2}
      y={card.active ? y : window.innerHeight*2}
      deg={deg || card.defaultDeg}
      scale={scale}
      center={center}
      padding={10}
      onTouchStart={(e) => playable && this.start(e)}
      onMouseDown={(e) => playable && this.start(e)}
      maxWidth={maxWidth}
      {...card}>
    {card && <React.Fragment>
      <Left
        padding={20}
        color={color}>{card.index}</Left>
        {this.getIcon(color, 'left')}
      <TextWrapper>
        <Title color={color}>{card.type}</Title>
        <Description color={color}>{card.rule}</Description>
      </TextWrapper>
      {this.getIcon(color, 'right')}
      <Right
        padding={20}
        color={color}>{card.index}</Right>
    </React.Fragment>}
  </CardWrapper>;
  }
}

export default Card;
