import React from 'react';
import Constants from '../../../config/constants';
import { ConstantsContext, Provider, Consumer, withConstants } from './Context';

const withSsrConstants = Page => {
  const WithSsrConstants = props => <Page {...props} />;

  WithSsrConstants.getInitialProps = async (props) => {
    const ConstantsName = props.ctx.query && props.ctx.query.ConstantsName ? props.ctx.query.ConstantsName : null;

    return {
      ...(Page.getInitialProps ? await Page.getInitialProps(props) : {}),
      ConstantsName
    };
  };

  WithSsrConstants.displayName = `WithSsrConstants(${Page.displayName || Page.name || 'Unknown'})`;

  return WithSsrConstants;
};

export {
  withSsrConstants as with,
  ConstantsContext,
  Provider,
  Consumer,
  withConstants
};
