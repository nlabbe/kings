import React from 'react';
import constants from '../../../config/constants';

const ConstantsContext = React.createContext({
  cards: null,
  colors: null,
  face: null
});

export class Provider extends React.PureComponent {

  state = {
    cards: null,
    colors: null,
    face: null
  }

  static getDerivedStateFromProps(props, state) {
    return {
      constants: constants
    };
  }

  render() {
    const { children } = this.props;
    return (
      <ConstantsContext.Provider value={this.state}>
        {children}
      </ConstantsContext.Provider>
    )
  }
}

export const Consumer = ConstantsContext.Consumer;

export function withConstants(Component) {
  return function ConstantsComponent(props) {
    return (
      <ConstantsContext.Consumer>
        {constantsProps => <Component {...props} {...constantsProps} />}
      </ConstantsContext.Consumer>
    );
  };
}
