import React from 'react';
import io from 'socket.io-client';

const RoomContext = React.createContext({
  turn: () => {},
  replay: () => {},
  cards: null,
  roomName: '',
  room: {},
  subscribe: false,
  subscribed: false
});

export class Provider extends React.PureComponent {

  componentDidMount() {
    const socket = io();
    this.setState({ socket });
    socket.on('room', this.handleRoom);
    socket.emit('roomName', this.props.roomName);
  }

  state = {
    turn: (e, id) => {
      const { roomName } = this.props;
      const { room } = this.state;
      e.preventDefault();

      this.state.socket.emit('turn', {roomName: roomName, id: room.turn.id, cardId: id});
    },
    replay: (e) => {
      const { roomName } = this.props;
      e.preventDefault();

      this.setState({ cards: null });
      this.state.socket.emit('replay', {roomName: roomName});
    },
    cards: null,
    roomName: '',
    room: {},
    socket: null
  }

  componentWillUnmount() {
    this.state.socket.off('room', this.handleRoom);
    this.state.socket.close();
  }

  handleRoom = (room) => {
    console.log('handleRoom', room);
    let cards = room && room.deck && room.deck.cards && room.deck.cards;
    this.setState({ room, cards });
  }

  static getDerivedStateFromProps(props, state) {
    return {
      roomName: props.roomName
    };
  }

  render() {
    const { children } = this.props;
    return (
      <RoomContext.Provider value={this.state}>
        {children}
      </RoomContext.Provider>
    )
  }
}

export const Consumer = RoomContext.Consumer;

export function withRoom(Component) {
  return function RoomComponent(props) {
    return (
      <RoomContext.Consumer>
        {roomProps => <Component {...props} {...roomProps} />}
      </RoomContext.Consumer>
    );
  };
}
