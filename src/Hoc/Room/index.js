import React from 'react';
import { RoomContext, Provider, Consumer, withRoom } from './Context';

const withSsrRoom = Page => {
  const WithSsrRoom = props => <Page {...props} />;

  WithSsrRoom.getInitialProps = async (props) => {
    const roomName = props.ctx.query && props.ctx.query.roomName ? props.ctx.query.roomName : null;

    return {
      ...(Page.getInitialProps ? await Page.getInitialProps(props) : {}),
      roomName
    };
  };

  WithSsrRoom.displayName = `WithSsrRoom(${Page.displayName || Page.name || 'Unknown'})`;

  return WithSsrRoom;
};

export {
  withSsrRoom as with,
  RoomContext,
  Provider,
  Consumer,
  withRoom
};
