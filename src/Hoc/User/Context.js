import React from 'react';
import cookie from 'cookie';

const UserContext = React.createContext({
  user: null,
  changeName: () => {},
  changeColor: () => {}
});

export class Provider extends React.PureComponent {

  state = {
    user: null,
    changeColor: (color) => this.update({ ...this.state.user, color }),
    changeName: (name) => this.update({ ...this.state.user, name }),
    changeAvatar: (avatar) => this.update({ ...this.state.user, ...avatar })
  }

  update(user) {
    document.cookie = cookie.serialize('user', JSON.stringify(user), { maxAge: 60*60*24*365 });
    this.setState({ user });
  }

  componentDidMount() {
    if(typeof window !== 'undefined' && !this.props.user) {
      const cookies = cookie.parse(document.cookie);
      this.setState({ user: JSON.parse(cookies.user) });
    }
  }

  static getDerivedStateFromProps(props, state) {
    let { user } = props;
    if(!state.user && user) {
      return { user };
    }

    return null;
  }

  render() {
    const { children } = this.props;
    return (
      <UserContext.Provider value={this.state}>
        {children}
      </UserContext.Provider>
    )
  }
}

export const Consumer = UserContext.Consumer;

export function withUser(Component) {
  return function RoomComponent(props) {
    return (
      <UserContext.Consumer>
        {userProps => <Component {...props} {...userProps} />}
      </UserContext.Consumer>
    );
  };
}
