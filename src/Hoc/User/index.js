import React from 'react';
import cookies from 'next-cookies';
import { UserContext, Provider, Consumer, withUser } from './Context';

const withSsrUser = Page => {
  const WithSsrUser = props => <Page {...props} />;

  WithSsrUser.getInitialProps = async (props) => {
    let { user } = cookies(props.ctx);
    user = user ? JSON.parse(user) : null;

    return {
      ...(Page.getInitialProps ? await Page.getInitialProps(props) : {}),
      user
    };
  };

  WithSsrUser.displayName = `WithSsrUser(${Page.displayName || Page.name || 'Unknown'})`;

  return WithSsrUser;
};

export {
  withSsrUser as with,
  UserContext,
  Provider,
  Consumer,
  withUser
};
