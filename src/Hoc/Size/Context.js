import React from 'react';

const SizeContext = React.createContext({
  width: 0,
  height: 0
});

export class Provider extends React.PureComponent {

  constructor(props) {
    super(props);
    typeof window !== 'undefined' && window.addEventListener('resize', () => this.resize());
  }

  state = {
    width: 0,
    height: 0
  }

  getSize() {
    const { innerWidth, innerHeight } = window;
    return {
      width: innerWidth > innerHeight ? innerWidth : innerHeight,
      height: innerWidth > innerHeight ? innerHeight : innerWidth
    };
  }

  resize() {
    this.setState({ ...this.getSize() });
  }

  componentDidMount() {
    if(this.state.width === 0) this.setState({ ...this.getSize() })
  }


  render() {
    const { children } = this.props;
    return (
      <SizeContext.Provider value={this.state}>
        {children}
      </SizeContext.Provider>
    )
  }
}

export const Consumer = SizeContext.Consumer;

export function withSize(Component) {
  return function RoomComponent(props) {
    return (
      <SizeContext.Consumer>
        {SizeProps => <Component {...props} {...SizeProps} />}
      </SizeContext.Consumer>
    );
  };
}
