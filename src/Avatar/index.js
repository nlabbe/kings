import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: ${({ center }) => center ? 'block' : 'inline-block'};
  margin: 0 ${({ center }) => center ? 'auto' : '0'};
  position: relative;
  background: #${({ color }) => color};
  width: ${({width}) => width ? `${width}px` : 'auto'};
  height: ${({height}) => height ? `${height}px` : 'auto'};
  ${({ radius }) => `border-radius: ${radius}px`};
  overflow: hidden;
  transition: transform 1s ease;
  width: 100%;

  &:before {
    content: ' ';
  }
`;

// const Img = styled.img`
//   width: 75%;
//   height: 75%;
//   top: 50%;
//   left: 50%;
//   transform: translate(-50%, -50%);
//   position: absolute;
// `;

const Img = styled.img`
  position: relative;
  width: 100%;
`;

export default ({ active, size, eyes, nose, mouth, color, center, onClick, width, height }) => <Wrapper
  onClick={() => onClick && onClick()}
  size={size}
  active={active}
  width={width}
  height={height}
  color={color && color.code}
  center={center}>
  <Img size={size} src={`https://api.adorable.io/avatars/face/${eyes}/${nose}/${mouth}/ffffff00/${width}`} />
</Wrapper>;
