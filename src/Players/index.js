import React from 'react';
import Avatar from '../Avatar';
import { List } from './Styles';
import * as RoomCtx from '../Hoc/Room';
import * as UserCtx from '../Hoc/User';

class Game extends React.PureComponent {
  static defaultProps = {
  }

  state = {
  }

  render() {
    const { user, room } = this.props;

    return <List>
        {room && room.users && Object.keys(room.users).map((id) => <Avatar
          active={room.turn && room.turn.id && room.turn.id === id}
          size={50}
          key={id}
          {...room.users[id]} />)}
      </List>;
  }
}

export default UserCtx.withUser(RoomCtx.withRoom(Game));
