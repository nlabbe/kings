import styled from 'styled-components';

export const List = styled.div`
  user-select: none;
  position: absolute;
  width: 100%;
  bottom: 0;
  padding: 10px;
`;
