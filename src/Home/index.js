import React from 'react';
import * as UserCtx from '../Hoc/User';
import * as ConstantsCtx from '../Hoc/Constants';
import * as SizeCtx from '../Hoc/Size';
import { Input, Button } from '../Common/Form';
import Background from '../Common/Background';
import Color from '../Common/Color';
import Avatar from '../Avatar';
import { ColorWrapper, AvatarWrapper } from './Styles';
import { Link } from '../../server/routes'

const random = function(arr) {
  return arr[Math.floor(Math.random()*arr.length)];
};

class Home extends React.PureComponent {
  static defaultProps = {
  }

  state = {
    size: null,
    room: null
  }

  componentDidMount() {
    if(typeof window !== 'undefined' && !this.state.size) this.setState({ size: Math.floor(window.innerWidth/2) });
  }

  static getDerivedStateFromProps(props, state) {
    return { user: props.user };
  }

  inputRoom = null
  input = null

  render() {
    const { constants, width } = this.props;
    const { size, room, user } = this.state;

    return <React.Fragment>
      <Background color={user && user.color.code}>
        <Input
          type="text"
          onChange={(e) => this.props.changeName(e.target.value)}
          light={user && user.color.light}
          value={user && user.name ? user.name : ''} />
        <AvatarWrapper>{size !== null && <Avatar
          width={width / 2}
          onClick={() => this.props.changeAvatar({
            eyes: random(this.props.constants.face.eyes),
            nose: random(this.props.constants.face.nose),
            mouth: random(this.props.constants.face.mouth)
          })}
          {...user}
          center />}
        </AvatarWrapper>
        <Input
          onChange={() => this.setState({ room: this.inputRoom.value })}
          innerRef={(node) => this.inputRoom = node}
          placeholder="Room name..."
          light={user && user.color.light} />
        <Link route={`/room/${room}`}>
          <Button>Play</Button>
        </Link>
        <ColorWrapper>
          {constants && constants.face && constants.face.color && constants.face.color.map((color, i) =>
            <Color
              key={i}
              color={color}
              onClick={() => this.props.changeColor(color)} />)}
        </ColorWrapper>
      </Background>}
    </React.Fragment>;
  }
}

export default SizeCtx.withSize(ConstantsCtx.withConstants(UserCtx.withUser(Home)));
