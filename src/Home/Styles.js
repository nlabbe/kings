import styled from 'styled-components';

export const AvatarWrapper = styled.div`
  margin: 0px auto;
  width: 50%;
  height: 50%;
`;

export const ColorWrapper = styled.div`
  position: absolute;
  display: flex;
  bottom: 0;
  width: 100%;
`;
