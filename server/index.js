const app = require('express')();
const server = require('http').Server(app);
const next = require('next');
const compression = require('compression');
const routes = require('./routes');
const path = require('path');
const cookieParser = require('cookie-parser');
const newUser = require('./user');
// const avatarsMiddleware = require('adorable-avatars').default;
require('./socket')(server);

const port = parseInt(process.env.PORT, 10) || 8000;
const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
// const nextHandler = nextApp.getRequestHandler()
const handler = routes.getRequestHandler(nextApp);

nextApp.prepare().then(() => {
  app.use(cookieParser());
  app.use((req, res, next) => {
    let user = req.cookies.user;
    if (user === undefined) {
      user = newUser();
      res.cookie('user', user, { maxAge: 60*60*24*365 });
    }
    return next();
  });

  // app.use('/avatar', avatarsMiddleware);
  if(!dev) {
    app.use(compression());
  }

  app.get('/service-worker.js', (req, res) => nextApp.serveStatic(req, res, path.join(process.cwd(), '.next', 'service-worker.js')));

  app.get('*', (req, res) => {
    return handler(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  });
});
