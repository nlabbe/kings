const socketIo = require('socket.io');
const cookie = require('cookie');
const rooms = require('./rooms');

module.exports = function(server) {
  const io = socketIo(server);

  // socket.io server
  io.on('connection', socket => {
    var cookies = cookie.parse(socket.request.headers.cookie);
    const user = JSON.parse(cookies.user);
    user.socketId = socket.id;

    socket.on('roomName', function(room) {
      rooms.leave(user);
      rooms.create(room, user);
      rooms.enter(room, user);
      socket.join(room);
      io.sockets.in(room).emit('room', rooms.findByName(room));
    });

    socket.on('turn', function ({ roomName, id, cardId }) {
      io.sockets.in(roomName).emit('room', rooms.nextUser(roomName, id, cardId));
    });

    socket.on('replay', function ({ roomName }) {
      console.log('replay', roomName);
      io.sockets.in(roomName).emit('room', rooms.replay(roomName));
    });

    socket.on('disconnect', function () {
      io.sockets.in(user.room).emit('room', rooms.leave(user));
    });

    // socket.on('error', function () {
    //   console.log('error', arguments);
    // });
  });
};
