const uuidv4 = require('uuid/v4');
const constants = require('../config/constants');

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function deck() {
  const game = [];
  constants.colors.map((color) => {
    constants.cards.map((card) => {
      const deg = getRandomInt(4) === 0 ? getRandomInt(10) - 7 : 0;
      game.push({...card, ...color, id: uuidv4(), active: true, defaultDeg: deg})
    });
  });

  const party = {
    cards: game,
    suffle: () => {
      for (let i = party.cards.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [party.cards[i], party.cards[j]] = [party.cards[j], party.cards[i]]; // eslint-disable-line no-param-reassign
      }

      const cards = {};
      party.cards.map((card) => {
        cards[card.id] = card;
      });
      party.cards = cards;

      return party;
    },
    turn: (id) => delete party.cards[id],
    count: () => party.cards.length,
    finished: () => party.cards.length <= 0
  };

  return party;
}

module.exports = deck;
