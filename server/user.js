const uuidv4 = require('uuid/v4');
const random_name = require('node-random-name');
const constants = require('../config/constants');

const random = function(arr) {
  return arr[Math.floor(Math.random()*arr.length)];
};

module.exports = function() {
  return JSON.stringify({
    id: uuidv4(),
    name: random_name(),
    eyes: random(constants.face.eyes),
    nose: random(constants.face.nose),
    mouth: random(constants.face.mouth),
    color: random(constants.face.color)
  });
};
