const rooms = {};
const deck = require('./deck');

const roomManager = {
  create: (name, user) => {
    if(!rooms[name]) {
      rooms[name] = {
        users: {},
        turn: user,
        deck: deck().suffle()
      };
    }
    return rooms[name];
  },
  replay: (name) => {
    if(!rooms[name]) {
      roomManager.create();
    }else {
      rooms[name].deck = deck().suffle();
    }
    return roomManager.nextUser(name);
  },
  leave: (user) => {
    const name = user.room;
    if(user.room && rooms[name] && rooms[name].users && rooms[name].users[user.id]) {
      delete rooms[name].users[user.id];
    }
    return rooms[name];
  },
  enter: (name, user) => {
    user.room = name;
    if(!rooms[name][user.id]) {
      rooms[name].users[user.id] = user;
    }
    return rooms[name];
  },
  findByName: (name) => rooms[name],
  nextUser: (name, id, cardId) => {
    if(rooms && rooms[name] && rooms[name].users) {
      let found = false;
      let user = null;
      Object.keys(rooms[name].users).map(function(i) {
        const currentUser = rooms[name].users[i];
        if(found) {
          user = currentUser;
          found = false;
        }
        if(id === currentUser.id) {
          found = true;
        }
      });
      if(!user) {
        user = rooms[name].users[Object.keys(rooms[name].users)[0]];
      }
      rooms[name].turn = user;
      rooms[name].card = rooms[name].deck.turn(cardId);
      rooms[name].count = rooms[name].deck.count();
      rooms[name].end = rooms[name].deck.finished();

      return rooms[name];
    }
    return rooms[name];
  }
};

module.exports = roomManager;
