const withPlugins = require('next-compose-plugins');
const css = require('@zeit/next-css');
const sass = require('@zeit/next-sass');
const commonsChunkConfig = require('@zeit/next-css/commons-chunk-config');
const optimizedImages = require('next-optimized-images');

const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
require('dotenv').config();
// const packgeJSON = require('./package.json');

// const dev = process.env.NODE_ENV !== 'production';

const nextConfig = {
  distDir: '.next',
  publicRuntimeConfig: process.env,
  webpack: (config, { dev, isServer }) => {

    if (!isServer) {
      config = commonsChunkConfig(config, /\.(sass|scss|css)$/);
    }

    if(!dev) {
      config.plugins.push(
        new SWPrecacheWebpackPlugin({
          navigateFallback: '/index',
          navigateFallbackWhitelist: [/^(?!\/__|\/api).*/],
          verbose: true,
          staticFileGlobsIgnorePatterns: [/\.next\//],
          ignoreUrlParametersMatching: [/\/api\//],
          runtimeCaching: [
            {
              handler: 'networkFirst',
              urlPattern: /^https?.*/
            }
          ]
        })
      );
    }

    return config;
  },
};

const initExport = withPlugins([
  [optimizedImages, {
    imagesOutputPath: 'static/images/',
    imagesPublicPath: '/_next/',
    inlineImageLimit: 8192,
    imagesFolder: 'images',
    imagesName: '[name]-[hash].[ext]',
    optimizeImagesInDev: false,
    mozjpeg: {
      quality: 80,
    },
    optipng: {
      optimizationLevel: 3,
    },
    pngquant: false,
    gifsicle: {
      interlaced: true,
      optimizationLevel: 3,
    },
    svgo: {
      // enable/disable svgo plugins here
    }
  }],
  sass,
  css
], nextConfig);

/* eslint-enable global-require */
module.exports = initExport;
