import React from 'react';
import Game from '../src/Game';
import Home from '../src/Home';
import * as ConstantsCtx from '../src/Hoc/Constants';
import * as RoomCtx from '../src/Hoc/Room';
import * as UserCtx from '../src/Hoc/User';
import * as SizeCtx from '../src/Hoc/Size';

class Index extends React.Component {
  static defaultProps = {
  }

  state = {
  }

  render() {
    const { roomName, user } = this.props;

    return (
      <ConstantsCtx.Provider>
        <SizeCtx.Provider>
          <UserCtx.Provider user={user}>
            {roomName
              ? <RoomCtx.Provider roomName={roomName}>
                <Game />
              </RoomCtx.Provider>
              : <Home />}
          </UserCtx.Provider>
        </SizeCtx.Provider>
      </ConstantsCtx.Provider>
    )
  }
}

export default Index;
