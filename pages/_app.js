import App, { Container } from 'next/app';
import React from 'react';
import styledNormalize from 'styled-normalize';
import { injectGlobal } from 'styled-components';
import getConfig from 'next/config';
const { publicRuntimeConfig } = getConfig();
import * as Room from '../src/Hoc/Room';
import * as Constants from '../src/Hoc/Constants';
import * as User from '../src/Hoc/User';
import '../config/polyfills.js'

injectGlobal`
  ${styledNormalize}
`;

class MyApp extends App {
  static async getInitialProps ({ Component, ctx, query }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  componentDidMount () {
    if(publicRuntimeConfig.NODE_ENV === 'production') {
      if ('serviceWorker' in navigator) {
        navigator.serviceWorker
          .register('/service-worker.js')
          .then(registration => {
            console.log('service worker registration successful', registration);
          })
          .catch(err => {
            console.warn('service worker registration failed', err.message);
          });
      }
    }
  }

  render () {
    const { Component, pageProps, roomName, user } = this.props;

    return (
      <Container>
          <Component {...pageProps} roomName={roomName} user={user} />
      </Container>
    )
  }
}

export default User.with(Room.with(Constants.with(MyApp)));
